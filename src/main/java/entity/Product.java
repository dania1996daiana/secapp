package entity;

import jakarta.persistence.Entity;

@Entity
public class Product {
	private Long id;
	private String detalle;
	private String marca;
	private Double precio_venta1;
	private Double precio_venta2;
	private Double precio_venta3;

	public Product(Long id, String detalle, String marca, Double precio_venta1, Double precio_venta2,
			Double precio_venta3) {
		this.id = id;
		this.detalle = detalle;
		this.marca = marca;
		this.precio_venta1 = precio_venta1;
		this.precio_venta2 = precio_venta2;
		this.precio_venta3 = precio_venta3;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Double getPrecio_venta1() {
		return precio_venta1;
	}

	public void setPrecio_venta1(Double precio_venta1) {
		this.precio_venta1 = precio_venta1;
	}

	public Double getPrecio_venta2() {
		return precio_venta2;
	}

	public void setPrecio_venta2(Double precio_venta2) {
		this.precio_venta2 = precio_venta2;
	}

	public Double getPrecio_venta3() {
		return precio_venta3;
	}

	public void setPrecio_venta3(Double precio_venta3) {
		this.precio_venta3 = precio_venta3;
	}

}
