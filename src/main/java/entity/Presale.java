package entity;

import java.sql.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

@Entity
public class Presale {
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date fecha;

	private Preventive preventive;
	private Customer customer;
	private Double total;

	public Presale (Long id, Date fecha, Preventive preventive, Customer customer, Double total) {
		this.id = id;
		this.fecha = fecha;
		this.preventive = preventive;
		this.customer = customer;
		this.total = total;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Preventive getPreventive() {
		return preventive;
	}

	public void setPreventive(Preventive preventive) {
		this.preventive = preventive;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

}
