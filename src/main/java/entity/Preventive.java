package entity;

import jakarta.persistence.Entity;

@Entity
public class Preventive {
	private Long id;
	private String nombre;

	public Preventive(Long id, String nombre) {
		this.id = id;
		this.nombre = nombre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
