package entity;

import jakarta.persistence.Entity;

//definir nombre de tabla
@Entity
//EL nombre de la clase de be esta en camelCase PresaleDetail
public class Presale_detail {
	//definir anotacion de realcion
	private Presale presale;
	private Product product;
	//nombre de atrubutos en ingles
	private Long cantidad;
	private Double precio_unitario;
	private Double precio_total;
	

	public Presale_detail(Presale presale, Product product, Long cantidad, Double precio_unitario, Double precio_total,
			String abm) {
		super();
		this.presale = presale;
		this.product = product;
		this.cantidad = cantidad;
		this.precio_unitario = precio_unitario;
		this.precio_total = precio_total;
		
	}

	public Presale getPresale() {
		return presale;
	}

	public void setPresale(Presale presale) {
		this.presale = presale;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecio_unitario() {
		return precio_unitario;
	}

	public void setPrecio_unitario(Double precio_unitario) {
		this.precio_unitario = precio_unitario;
	}

	public Double getPrecio_total() {
		return precio_total;
	}

	public void setPrecio_total(Double precio_total) {
		this.precio_total = precio_total;
	}

	

}
